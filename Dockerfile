FROM centos:7

RUN yum -y groupinstall 'Development Tools'
RUN mkdir -p /mud/bin

ADD driver-148p2 /src/driver
ADD x-erq1.4 /src/erq

WORKDIR /src/driver/
RUN ./configure && make && cp driver /mud/bin/parse

WORKDIR /src/erq
RUN ./Configure <<< $'/src/driver\nn\nn\n' && make && cp erq /mud/bin/erq

VOLUME /mud/mudlib
EXPOSE 4711
WORKDIR /mud/

CMD ["/mud/bin/parse", "-m/mud/mudlib"]